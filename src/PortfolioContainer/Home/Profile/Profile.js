import React from "react";
import Typical from "react-typical";
import "./Profile.css";

export default function Profile() {
  return (
    <div className="profile-container">
      <div className="profile-parent">
        <div className="profile-details">
          <div className="colz">
            <div className="colz-icon">
              <a href="#">
                <i className="fa fa-facebook"></i>
              </a>
              <a href="#">
                <i className="fa fa-google"></i>
              </a>
              <a href="#">
                <i className="fa fa-instagram"></i>
              </a>
              <a href="#">
                <i className="fa fa-telegram"></i>
              </a>
            </div>
          </div>

          <div className="profile-detail-name">
            <span className="primary-text">
              {""} Hello, I'm{" "}
              <span className="hightlight-text">RORTHA</span>
            </span>
          </div>
          <div className="profile-detail-role">
            <span className="primary-text">
              {""}
              <h1>
                {" "}
                <Typical
                  loop={Infinity}
                  steps={[
                    "Hello Rortha",
                    1000,
                    "Working on time",
                    1000,
                    "Happy new year",
                    1000,
                    "Happy birthday",
                    1000,
                    "Happy Ending year",
                    1000,
                  ]}
                />
              </h1>
              <span className="profile-role-tagline">
                Knack of your building application with front end and backend
              </span>
            </span>
          </div>
          <div className="profile-options">
            <button className="btn primary-btn">
              {""}
              Hire Me{" "}
            </button>
            <a href="ehizcv.pdf" download="profile ehizcv.pdf">
              <button className="btn highlighted-btn ">Get Resume</button>
            </a>
          </div>
        </div>
        <div className="profile-picture">
          <div className="profile-picture-background"></div>
        </div>
      </div>
    </div>
  );
}
