import React from "react";
import "./Footer.css";
import shape from '../../../assets/home/shape-bg.png';
export default function Footer() {
  return (
    <div className="footer-container">
      <div className="footer-parent">
        <img src={shape} alt='internet no connection' />
      </div>
    </div>
  );
}
